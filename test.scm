#!/usr/local/bin/csi -s

(include "tiled")
(import scheme (chicken base) tiled (chicken pretty-print))

(pp (load-tiled-file "example.json"))

(define tmap (load-tiled-map "example.json"))
(display tmap)
(display (map-name tmap))
