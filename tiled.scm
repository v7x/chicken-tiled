#!/usr/local/bin/csi -s

(module tiled

 *

 (import scheme (chicken base) (chicken pretty-print) (chicken string)
  medea coops coops-primitive-objects coops-utils srfi-69)
 
 (define alist-ref-null
  (lambda (key alist)
   (let ((ref (alist-ref key alist)))
   (if (equal? ref #f)'()
    ref))))

 (define parse-color
  (lambda (color)
   (string->number
    (string-translate color "#" "#x"))))

 (define-class tiled ()
  ((id reader: tiled-id)
   (properties reader: properties)))

 (define-class tiled-map (tiled)
  ((name reader: map-name)
   (height reader: map-height)
   (width reader: map-width)
   (backgroundcolor reader: background-color)
   (layers reader: map-layers)
   (tilesets reader: map-tile-sets)))

 ;tiled map layouts

 (define-class orthogonal-map (tiled-map)
  ((renderorder reader: render-order)))

 (define-class isometric-map (tiled-map))

 (define-class staggered-map (tiled-map)
  ((staggeraxis reader: stagger-axis)
   (staggerindex reader: stagger-index)))

 (define-class hexagonal-map (tiled-map)
  ((hexsidelength reader: hex-side-length)
   (staggeraxis reader: stagger-axis)
   (staggerindex reader: stagger-index)))

 ;tileset

 (define-class tiled-tileset (tiled)
  ((name reader: tileset-name)
   (firstgid reader: first-gid)
   (tilewidth reader: tileset-width)
   (tileheight reader: tileset-height)
   (tiles reader: tiles)
   (spacing reader: tileset-spacing)
   (margin reader: tileset-margin)
   (columns reader: tileset-columns)
   (terrain reader: terrain-sets)
   (wang reader: wang-sets)))

 (define define-tileset
  (lambda (tileset-data)
   (let* ((tileset (load-tiled-file (alist-ref-null 'source tileset-data)))
          (terrains (alist-ref-null 'terrains tileset))
          (wangs (alist-ref-null 'wangsets tileset))
          (propertys (alist-ref-null 'properties tileset)))
    (make tiled-tileset name (alist-ref-null 'name tileset)
                        first-gid (alist-ref-null 'firstgid tileset)
                        tileset-width (alist-ref-null 'tilewidth tileset)
                        tileset-height (alist-ref-null 'tileheight tileset)
                        tiles (make-tile-list
                               (alist-ref-null 'tiles tileset))
                        tileset-spacing (alist-ref-null 'spacing tileset)
                        tileset-margin (alist-ref-null 'margin tileset)
                        tileset-columns (alist-ref-null 'columns tileset)
                        terrain-sets (if (eqv? '() terrains) '()
                                    (make-terrain-sets 
                                     (alist-ref-null 'terrains tileset)))
                        wang-sets (if (eqv? '() wangs) '()
                                 (define-wang-sets 
                                  (alist-ref-null 'wangsets tileset)))
                        properties (define-properties 
                                    (alist-ref-null 'properties tileset))))))

 (define make-tilesets
  (lambda (data)
   (map define-tileset data)))

 ;layer types: tile layers and object groups

 (define-class layer (tiled)
  ((x accessor: x-offest)
  (y accessor: y-offset)))

 (define-class tile-layer (layer)
  ((tile-data reader: tile-data)
   (height reader: layer-height)
   (width reader: layer-width)))

 (define-class object-layer (layer)
  ((name reader: object-layer-name)
   (draworder reader: draw-order)
   (objects reader: objects)))

 ;terrain

 (define-record-type terrain
  (make-terrain name properties)
  terrain?
  (name terrain-name)
  (properties terrain-properties))

 (define-method (define-terrain (name <symbol>)
                                (properties <hash-table>))
  (make-terrain name properties))

 (define-primitive-class <terrain> (<record>) terrain?)

 (define-primitive-class terrain-set (<list>) (list-of? terrain?))
 
 (define define-terrain-set
  (lambda (data)
   (map
    (lambda (terrain-data)
     (define-terrain (string->symbol (alist-ref-null 'name terrain-data))
                     (define-properties (alist-ref-null 'properties terrain-data))))
    data)))

 (define make-terrain-sets
  (lambda (data)
   (map define-terrain-set data)))

 ;wang sets and tiles

 (define-record-type wang-tile
  (make-wang-tile tileid wangid)
  wang-tile?
  (tileid tile-id)
  (wangid wang-id)
  (properties properties))

 (define-primitive-class <wang-tile> (<record>) wang-tile?)

 (define-record-type wang-set
  (make-wang-set name cornercolors edgecolors tiles properties)
  wang-set?
  (name wang-set-name)
  (cornercolors corner-colors)
  (edgecolors edge-colors)
  (tiles wang-set-tiles)
  (properties properties))

 (define-primitive-class <wang-set> (<record>) wang-set?)

 (define define-wang-sets
  (lambda (data)
   (map
    (lambda (wang-data)
     (make-wang-set (string->symbol (alist-ref-null 'name wang-data))
                    (make-wang-colors
                     (alist-ref-null 'cornercolors wang-data))
                    (make-wang-colors
                     (alist-ref-null 'edgecolors wang-data))
                    (define-wang-tiles (alist-ref 'wangtiles wang-data))
                    (define-properties
                     (alist-ref-null 'properties wang-data))))
    data)))

 (define make-wang-colors
  (lambda (data)
   (map
    (lambda (color-data)
     (parse-color (alist-ref 'color color-data)))
    data)))

 (define define-wang-tiles
  (lambda (data)
   (map
    (lambda (wang-data)
     (make-wang-tile (alist-ref-null 'tileid wang-data)
                     (list->vector
                      (alist-ref-null 'wangid wang-data))))
    data)))
                     
 ;tiles, both regular and objects

 (define-class tile (tiled)
  ((terrain reader: tile-terrain)
   (image reader: tile-image)
   (type reader: tile-type)))

 (define define-tile
  (lambda (data)
   (make tile tiled-id (alist-ref-null 'id data)
              tile-type (string->symbol (alist-ref-null 'type data))
              terrain (list->vector (alist-ref-null 'terrain data))
              tile-image (alist-ref-null 'image data)
              width (alist-ref-null 'width data)
              height (alist-ref-null 'height data)
              properties (define-properties
                          (alist-ref-null 'properties data)))))

 (define make-tile-list
  (lambda (data)
   (map define-tile data)))

 (define-class tiled-object (tiled)
  ((name reader: name)
  (type reader: object-type)
  (x reader: x-offset)
  (y reader: y-offset)
  (height reader: height)
  (width reader: width)
  (rotation reader: rotation)))

 (define-class elipse-object (tiled-object))

 (define make-elipse-object
  (lambda (data)
   (make elipse-object tiled-id (alist-ref-null 'id data)
                       name (string->symbol (alist-ref-null 'name data))
                       object-type (string->symbol (alist-ref-null 'type data))
                       x-offset (alist-ref-null 'x data)
                       y-offset (alist-ref-null 'y data)
                       height (alist-ref-null 'height data)
                       width (alist-ref-null 'width data)
                       rotation (alist-ref-null 'rotation data)
                       properties (define-properties
                                   (alist-ref-null 'properties data)))))

 (define-class point-object (tiled-object)
  ((x reader: x-coord)
   (y reader: y-coord)))

 (define make-point-object
  (lambda (data)
   (make point-object tiled-id (alist-ref-null 'id data)
                       name (string->symbol (alist-ref-null 'name data))
                       object-type (string->symbol (alist-ref-null 'type data))
                       x-coord (alist-ref-null 'x data)
                       y-coord (alist-ref-null 'y data)
                       height 0
                       width 0
                       rotation 0
                       properties (define-properties
                                   (alist-ref-null 'properties data)))))

 (define-class polygon-object (tiled-object)
  ((verticies reader: verticies)))

 (define make-polygon-object
  (lambda (data)
   (make polygon-object tiled-id (alist-ref-null 'id data)
                       name (string->symbol (alist-ref-null 'name data))
                       object-type (string->symbol (alist-ref-null 'type data))
                       verticies (list->vector (alist-ref-null 'polygon data))
                       x-offset (alist-ref-null 'x data)
                       y-offset (alist-ref-null 'y data)
                       height (alist-ref-null 'height data)
                       width (alist-ref-null 'width data)
                       rotation (alist-ref-null 'rotation data)
                       properties (define-properties
                                   (alist-ref-null 'properties data)))))

 (define-class line-object (tiled-object)
  ((verticies reader: verticies)))

 (define make-line-object
  (lambda (data)
   (make line-object tiled-id (alist-ref-null 'id data)
                       name (string->symbol (alist-ref-null 'name data))
                       object-type (string->symbol (alist-ref-null 'type data))
                       verticies (list->vector (alist-ref-null 'polyline data))
                       x-offset (alist-ref-null 'x data)
                       y-offset (alist-ref-null 'y data)
                       height (alist-ref-null 'height data)
                       width (alist-ref-null 'width data)
                       rotation (alist-ref-null 'rotation data)
                       properties (define-properties
                                   (alist-ref-null 'properties data)))))
 
 (define-class text-object (tiled-object)
  ((bold? reader: bold?)
  (color reader: text-color)
  (fontfamily reader: font-family)
  (halign reader: horizontal-align) ;one of 'center, 'right, 'justify, or 'left
  (italic? reader: italic?)
  (kerning? reader: kerning?)
  (pixelsize reader: pixel-size)
  (strikeout? reader: strikeout?)
  (text reader: text-content)
  (underline? reader: underline?)
  (valign reader: vertical-align) ;one of 'center, 'bottom, or 'top
  (wrap? reader: wrap?)))
 
 (define make-text-object
  (lambda (data)
   (make text-object tiled-id (alist-ref-null 'id data)
                       name (string->symbol (alist-ref-null 'name data))
                       object-type (string->symbol (alist-ref-null 'type data))
                       x-offset (alist-ref-null 'x data)
                       y-offset (alist-ref-null 'y data)
                       height (alist-ref-null 'height data)
                       width (alist-ref-null 'width data)
                       rotation (alist-ref-null 'rotation data)
                       bold? (alist-ref 'bold data)
                       text-color (parse-color (alist-ref-null 'color data))
                       font-family (alist-ref-null 'fontfamily data)
                       horizontal-align (alist-ref-null 'halign data)
                       italic? (alist-ref 'italic data)
                       kerning? (alist-ref 'kerning data)
                       pixel-size (alist-ref-null 'pixelsize data)
                       strikeout? (alist-ref 'strikeout data)
                       text-content (alist-ref-null 'text data)
                       underline? (alist-ref 'underline data)
                       vertical-align (alist-ref-null 'valign data)
                       wrap? (alist-ref-null 'wrap data)
                       properties (define-properties
                                   (alist-ref-null 'properties data)))))

 (define-class tile-object (tiled-object)
  ((gid reader: object-tile)))

 (define make-tile-object
  (lambda (data)
   (make tile-object tiled-id (alist-ref-null 'id data)
                       object-tile (alist-ref-null 'gid data)
                       name (string->symbol (alist-ref-null 'name data))
                       object-type (string->symbol (alist-ref-null 'type data))
                       x-offset (alist-ref-null 'x data)
                       y-offset (alist-ref-null 'y data)
                       height (alist-ref-null 'height data)
                       width (alist-ref-null 'width data)
                       rotation (alist-ref-null 'rotation data)
                       properties (define-properties
                                   (alist-ref-null 'properties data)))))

 (define-class rectangle-object (tiled-object))

 (define make-rectangle-object
  (lambda (data)
   (make rectangle-object tiled-id (alist-ref-null 'id data)
                       name (string->symbol (alist-ref-null 'name data))
                       object-type (string->symbol (alist-ref-null 'type data))
                       x-offset (alist-ref-null 'x data)
                       y-offset (alist-ref-null 'y data)
                       height (alist-ref-null 'height data)
                       width (alist-ref-null 'width data)
                       rotation (alist-ref-null 'rotation data)
                       properties (define-properties
                                   (alist-ref-null 'properties data)))))

 (define make-object-list
  (lambda (data)
   (map define-object data)))

 (define define-object
  (lambda (data)
   ; use alist-ref instead of alist-ref-null for things that arent point or
   ; bool, as they could be empty lists
   (cond (not (eqv? (alist-ref-null 'elipse data) '()))
         (make-elipse-object data)
         (not (eqv? (alist-ref 'point data) '()))
         (make-point-object data)
         (not (alist-ref 'polygon data))
         (make-polygon-object data)
         (not (alist-ref 'polyline data))
         (make-line-object data)
         (not (alist-ref 'text data))
         (make-text-object data)
         (not (alist-ref 'gid data))
         (make-tile-object data)
         (else (make-rectangle-object data)))))

 (define define-properties
  (lambda (prop-list)
   (define table (make-hash-table))
   (for-each (lambda (prop)
              (define name (string->symbol (alist-ref-null 'name prop)))
              (define typ (string->symbol (alist-ref-null 'type prop)))
              (case typ
               (('bool) (hash-table-set! table name (alist-ref-null 'value prop)))
               (('color) (hash-table-set! table name 
                          (parse-color (alist-ref-null 'value prop))))
               (('file) (hash-table-set! table name (alist-ref-null 'value prop)))
               (('float) (hash-table-set! table name (string->number 
                                                      (alist-ref-null 'value prop))))
               (('int) (hash-table-set! table name (string->number
                                                    (alist-ref-null 'value prop))))
               (('string) (hash-table-set! table name (alist-ref-null 'value prop)))))
    prop-list)
   table))

 ; makes json arrays into lists instead of vectors
 ; so that we can map/for-each over them them
 (define array-as-list-parser
  (cons 'array (lambda (x) x)))

 (json-parsers (cons array-as-list-parser (json-parsers)))

 (define load-tiled-file
  (lambda (path)
   (define file (open-input-file path))
   (define m (read-json file))
   (close-input-port file)
   m))
 
 (define load-tiled-map
  (lambda (path)
   (let ((map-data (load-tiled-file path)))
        (define-tiled-map (string->symbol path) map-data))))
 
 (define make-layers
  (lambda (data)
   (map
    (lambda (layer-data)
     (case (string->symbol (alist-ref-null 'type layer-data))
      (('tilelayer)
       (make tile-layer tiled-id (alist-ref-null 'id layer-data)
                        x-offset (alist-ref-null 'x layer-data)
                        y-offset (alist-ref-null 'y layer-data)
                        tile-data (list->vector (alist-ref-null 'data layer-data))
                        height (alist-ref-null 'height layer-data)
                        width (alist-ref-null 'width layer-data)
                        properties (define-properties (alist-ref-null 'properties
                                                                 layer-data))))
      (('objectgroup)
       (make object-layer tiled-id (alist-ref-null 'id layer-data)
                          x-offset (alist-ref-null 'x layer-data)
                          y-offset (alist-ref-null 'y layer-data)
                          name (string->symbol (alist-ref-null 'name layer-data))
                          draw-order (string->symbol (alist-ref-null 'draworder 
                                                               layer-data))
                          objects (make-object-list
                                   (alist-ref-null 'objects layer-data))))))
    data)))

 (define define-tiled-map
  (lambda (name map-data)
   (let ((layer-data (alist-ref-null 'layers map-data))
         (tileset-data (alist-ref-null 'tilesets map-data)))

    (case (string->symbol (alist-ref-null 'orientation map-data))
     (('orthogonal) 
      (make orthogonal-map map-name name
                           height (alist-ref-null 'height map-data)
                           width (alist-ref-null 'width map-data)
                           background-color (parse-color
                                            (alist-ref-null 'backgroundcolor
                                                            map-data))
                           render-order (string->symbol
                                        (alist-ref-null 'renderorder map-data))
                           map-layers (make-layers layer-data)
                           map-tile-sets (make-tilesets tileset-data)))
     (('isometric) 
      (make isometric-map map-name name
                          height (alist-ref-null 'height map-data)
                          width (alist-ref-null 'width map-data)
                           background-color (parse-color
                                            (alist-ref-null 'backgroundcolor
                                                            map-data))
                          map-layers (make-layers layer-data)
                          map-tile-sets (make-tilesets tileset-data)))
      
     (('staggered) 
      (make staggered-map map-name name
                          height (alist-ref-null 'height map-data)
                          width (alist-ref-null 'width map-data)
                          background-color (parse-color
                                            (alist-ref-null 'backgroundcolor
                                                            map-data))
                          map-layers (make-layers layer-data)
                          map-tile-sets (make-tilesets tileset-data)))
      
     (('hexagonal) 
      (make hexagonal-map map-name name
                           height (alist-ref-null 'height map-data)
                           width (alist-ref-null 'width map-data)
                           background-color (parse-color
                                            (alist-ref-null 'backgroundcolor
                                                            map-data))
                           hex-side-length (alist-ref-null 'hexsidelength map-data)
                           stagger-axis (alist-ref-null 'staggeraxis map-data)
                           stagger-index (alist-ref-null 'staggerindex map-data)
                           map-layers (make-layers layer-data)
                           map-tile-sets (make-tilesets tileset-data)))))))
)
